package academy.learnprogramming;

public class SelectionSort {

    public static void sort() {

        int[] intArray = { 20, 35, -15, 7, 55, 1, -22 };

        for (int lastUnsortedIndex = intArray.length - 1; lastUnsortedIndex > 0; lastUnsortedIndex--) {

            int indexOfLargest = 0;

            for (int i = 0; i <= lastUnsortedIndex; i++) {
                if (intArray[i] > intArray[indexOfLargest]) {
                    indexOfLargest = i;
                }
            }

            swap(intArray, indexOfLargest, lastUnsortedIndex);

        }

        for (int i = 0; i < intArray.length; i++) {
            System.out.println(intArray[i]);
        }

    }

    private static void swap(int[] array, int i, int j) {

        if (i == j) {
            return;
        }

        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;

    }

}
